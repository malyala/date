/* Program to read a date from input and write that date to output
. The user should be able to initilize a Date with data of that date
such as Day, Month and Year

Name :  Date.h
Author: Amit Malyala
Copyright: Amit Malyala
License: Freeware
Revision and bug history:
Version : 0.1
*/

#ifndef DATEINPUT_H
#define DATEINPUT_H

#include <iostream>
#include "std_types.h"

/* Days of the week */
#define SUNDAY 0
#define MONDAY 1
#define TUESDAY 2
#define WEDNESDAY 3
#define THURSDAY 4
#define FRIDAY 5
#define SATURDAY 6
/* Months */
#define JANUARY 1
#define FEBRUARY 2
#define MARCH 3
#define APRIL 4
#define MAY 5
#define JUNE 6
#define JULY 7 
#define AUGUST 8
#define SEPTEMBER 9
#define OCTOBER 10
#define NOVEMBER 11
#define DECEMBER 12

/* Date structure definition */
struct Date
{
    SINT32 Day;
    SINT32 Month;
    SINT32 Year;
};

// Get user to input a date
void GetDate(struct Date* d);

// Write Date to output
void WriteDates(const struct  Date* d);

// Initialize Date
void InitializeDate(struct Date* d);

/*
Component Function: SINT32 Dayofweek(SINT32 Day, SINT32 Month, SINT32 Year)
Arguments:  Integer parameters for day, month, year
Returns: Returns day of the week 
Description:  Calculate day of a week.
Version : 0.1
Notes:
*/
SINT32 Dayofweek(SINT32 Day, SINT32 Month, SINT32 Year);
/*
Component Function: SINT32 PrintWeekday(SINT32 Day)
Arguments:  Integer parameters for day
Returns: Returns day of the week 
Description: Prints weekday
Version : 0.1
Notes:
*/
void PrintWeekday(SINT32 Day);

/*
Component Function: SINT32 GetDaysOffset(const SINT32 &WeekDay)
Arguments:  a integer parameter for date
Returns: None
Description: 
Version : 0.1
Notes:
Function being designed
*/
SINT32 GetDaysOffset(const SINT32 &WeekDay);

/*
Component Function: void CalculateDateofFirstMondayGivenaDate(struct Date* d)
Arguments:  pointer to struct of a given date.
Returns: None
Description: Prints Date of first monday following a day.
Version : 0.1
Notes:
*/
void CalculateDateofFirstMondayGivenaDate(struct Date* d);

/*
Component Function: SINT32 GetNumberofDaysinMonth(const struct Date* d)
Arguments:  a pointer to struct Date.
Returns: None
Description: 
Version : 0.1
Notes:
Function being designed
*/
SINT32 GetNumberofDaysinMonth(const struct Date* d);

#endif //#ifndef 13_DATEINPUT_H