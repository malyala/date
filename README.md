# Date data structure

This project contains simple functionality to calculate day of a given date and calculate occurence of a day given the number of days after a date. 
This project uses gregorian calendar calculations. A class can also be used instead of a struct for storing date data. 